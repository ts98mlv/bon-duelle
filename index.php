<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use bonduelle\models\Personnage;

require __DIR__ . '/src/config/config.inc.php';
require __DIR__ . '/vendor/autoload.php';

// Create container
$container = array();


///////////
// TWIG //
//////////

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('src/views', ["debug" => true]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    $view->addExtension(new \Twig_Extension_Debug());

    return $view;
};


///////////////
// ELOQUENT //
//////////////
$container['settings'] = $config;
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

// Init Slim
$app = new \Slim\App($container);

//session_start
session_start();


//////////////
// ROUTAGE //
/////////////

$app->get('/','\\bonduelle\\controllers\\AuthentificationController:afficherConnexion')->setName('pageAuth');

$app->get('/nom', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Hello");
    return $response;
});

$app->get('/nom/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $name = strtoupper($name);
    $response->getBody()->write("Hello, $name.");
    return $response;
});


$app->get('/admin/listLegumes', '\\bonduelle\\controllers\\PersonnageController:indexLegume')->setName('listLegumes')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get('/admin/listFruits', '\\bonduelle\\controllers\\PersonnageController:indexFruit')->setName('listFruits')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get('/admin/modifPerso/{id}', '\\bonduelle\\controllers\\PersonnageController:modifAffichPerso')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get('/admin/modifMonstre/{id}','\\bonduelle\\controllers\\PersonnageController:modifAffichMonstre')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/modifUtilisateur/{id}","\\bonduelle\\controllers\\AdminController:modifAffichUtilisateur")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->post('/admin/modifMonstre', '\\bonduelle\\controllers\\PersonnageController:modifMonstre')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->post('/admin/modifPerso','\\bonduelle\\controllers\\PersonnageController:modifPerso')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get('/auth', '\\bonduelle\\controllers\\AuthentificationController:afficherConnexion');

$app->post("/login", '\\bonduelle\\controllers\\AuthentificationController:authentifier');

$app->get("/admin/creerCompte", "\\bonduelle\\controllers\\AdminController:creerCompte")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->post('/admin/creerCompte', "\\bonduelle\\controllers\\AdminController:ajouterCompte")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/home", "\\bonduelle\\controllers\\AuthentificationController:voirAccueilAdmin")->setName('accueilAdmin')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/joueur/home", "\\bonduelle\\controllers\\PersonnageController:getHome")->setName('accueilJoueur')->add(new \bonduelle\middlewares\JoueurSessionMiddleware());
$app->get("/joueur/choix1vs1", "\\bonduelle\\controllers\\PersonnageController:getChoix1vs1")->setName('choix1vs1')->add(new \bonduelle\middlewares\JoueurSessionMiddleware());
$app->get("/joueur/choix3vs3", "\\bonduelle\\controllers\\PersonnageController:getChoix3vs3")->setName('choix3vs3')->add(new \bonduelle\middlewares\JoueurSessionMiddleware());

$app->get("/admin/voirUtilisateurs", "\\bonduelle\\controllers\\AdminController:voirUtilisateurs")->setName('voirUtilisateurs')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/creerPersonnage", "\\bonduelle\\controllers\\AdminController:creerPersonnage")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/classement","\\bonduelle\\controllers\\StatsControlleur:creerClassement")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->post("/admin/creerHero", "\\bonduelle\\controllers\\AdminController:creerHero")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/creerMonstre", "\\bonduelle\\controllers\\AdminController:creerMonstre")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->post("/admin/creerMonstre", "\\bonduelle\\controllers\\AdminController:ajouterMonstre")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/deleteperso/{id}", '\\bonduelle\\controllers\\PersonnageController:DeletePerso')->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->post("/joueur/combat", "\\bonduelle\\controllers\\CombatController:creerCombat");

$app->post("/admin/modifUtilisateur","\\bonduelle\\controllers\\AdminController:modifUtilisateur")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/supprUtilisateur/{id}","\\bonduelle\\controllers\\AdminController:supprUtilisateur")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/listCombats","\\bonduelle\\controllers\\StatsControlleur:showCombats")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/statsMonstres","\\bonduelle\\controllers\\StatsControlleur:voirStatsMonstres")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get("/admin/statsPersos","\\bonduelle\\controllers\\StatsControlleur:voirStatsPersos")->add(new \bonduelle\middlewares\AdminSessionMiddleware());

$app->get('/joueur/getPerso/{id}', '\\bonduelle\\controllers\\PersonnageController:getPersoWithID');

$app->get('/joueur/addWinner/{idCombat}/{winnerType}','\\bonduelle\controllers\\CombatController:addWinner');

$app->post("/joueur/creerStatPerso", "\\bonduelle\\controllers\\StatsControlleur:creerStats");

$app->post("/joueur/combat3v3", "\\bonduelle\controllers\\CombatController:creerCombat3v3");

$app->run();
