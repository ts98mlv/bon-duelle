<?php

namespace bonduelle\middlewares;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;


class AdminSessionMiddleware extends BaseMiddleware {

    /**
     * méthode invoquée lors de l'utilisation du middleware
     * @param $request
     * @param $response
     * @param $next
     * @return ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {

        //si on est pas connecté on redirige sur la page de connexion
        if(! isset($_SESSION['user']) || empty($_SESSION['user'])){

            return $this->redirectAccueil($response);
        }

        $user_session = $_SESSION['user'];

        //si on est pas admin on redirige sur la page de connexion
        if( ! $user_session['admin']){
            return $this->redirectAccueil($response);
        }

        $response = $next($request, $response);

        return $response;
    }
}