<?php

namespace bonduelle\middlewares;

use Psr\Http\Message\ResponseInterface;


class BaseMiddleware {

    /**
     * méthode qui permet de rediriger vers la page d'accueil (connexion)
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function redirectAccueil(ResponseInterface $response)
    {
        return $response->withStatus(302)->withHeader('Location', '../');
    }

}