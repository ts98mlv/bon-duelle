<?php


namespace bonduelle\controllers;

use bonduelle\models\Authentification;
use bonduelle\models\Combat;
use bonduelle\models\Personnage;
use Psr\Http\Message\ResponseInterface;


class AdminController extends BaseController
{
    const ADMIN = 0;

    /**
     * méthode qui permet d'afficher la vue avec le formulaire pour créer un compte
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function creerCompte($request, $response)
    {

        return $this->render($response, 'Admin.creerCompte.html.twig');
    }

    /**
     * méthode privée qui permet de rajouter un grain de sel à des mots de passe
     * @param $password
     * @return false|string
     */
    private function saler($password)
    {
        return password_hash("La nature, notre futur" . $password, PASSWORD_DEFAULT);
    }

    /**
     * méthode qui permet d'ajouter un compte utilisateur en BDD
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function ajouterCompte($request, $response)
    {
        //recuperation des donnees du post
        $niveau = (isset($_POST['niveau'])) ? $_POST['niveau'] : null;
        $login = (isset($_POST['login'])) ? $_POST['login'] : null;
        $password = (isset($_POST['password'])) ? $_POST['password'] : null;

        try {

            //verification de l'existence des données
            if (!isset($niveau) || !isset($login) || !isset($password))
                throw new \Exception("Au moins une donnée requise n'a pas été rentrée");

            //Verification si utilisateur existe déjà
            if (!empty(Authentification::where("login", $login)->first())) {
                throw new \Exception("Nom d'utilisateur déjà utilisé!");
            }

            //traitement des données
            $niveau = filter_var($niveau, FILTER_SANITIZE_STRING);
            $login = filter_var($login, FILTER_SANITIZE_STRING);
            $password = filter_var($password, FILTER_SANITIZE_STRING);

            switch ($niveau) {
                case "Joueur":
                    $niveau = 1;
                    break;
                case "Administrateur":
                    $niveau = 0;
                    break;
                default:
                    $niveau = 1;
            }

            $password = $this->saler($password);

            //enregistrement en bdd
            $user = new Authentification();
            $user->login = $login;
            $user->password = $password;
            $user->niveau = $niveau;
            $user->save();

            //libération de la mémoire
            unset($niveau);
            unset($login);
            unset($password);
            unset($user);

            //redirections
            return $this->redirect($response, 'voirUtilisateurs');

        } catch (\Exception $e) {
            return $this->render($response, "Admin.creerCompte.html.twig", [erreur => $e->getMessage()]);
        }
    }

    /**
     * méthode qui permet d'afficher la liste des comptes utilisateurs
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function voirUtilisateurs($request, $response)
    {
        $users = Authentification::all();

        return $this->render($response, "Admin.voirUsers.html.twig", ["users" => $users]);
    }

    /**
     * méthode qui permet l'affichage de la vue qui permet de créer un personnage
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function creerPersonnage($request, $response)
    {
        return $this->render($response, "Admin.creerPersonnage.hero.html.twig");
    }

    /**
     * méthode qui permet d'insérer un héro en BDD
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function creerHero($request, $response)
    {
        //chemin où seront stockées les photos des personnages
        $cheminAvatar = $GLOBALS['PUBLIC'] . "images/personnages/";

        //récupération des données
        $nom = (isset($_POST['nom'])) ? $_POST["nom"] : null;
        $poids = (isset($_POST['poids'])) ? $_POST["poids"] : null;
        $taille = (isset($_POST['taille'])) ? $_POST["taille"] : null;
        $pv = (isset($_POST['pv'])) ? $_POST["pv"] : null;
        $pdef = (isset($_POST['pdef'])) ? $_POST["pdef"] : null;
        $pa = (isset($_POST['pa'])) ? $_POST["pa"] : null;
        $prenom = (isset($_POST['prenom'])) ? $_POST["prenom"] : null;
        $pAtt = (isset($_POST['pAtt'])) ? $_POST['pAtt'] : null;

        try {
            //verification des données
            if (!isset ($nom) || !isset ($prenom) || !isset ($poids) || !isset ($taille) || !isset ($pv) || !isset ($pdef) || !isset ($pa))
                throw new \Exception("Une donnée requise n'a pas été entrée !");

            $persoDouble = Personnage::where("nom",$nom)->first();
            if (!empty($persoDouble)){
                throw new \Exception('Nom déjà utilisé !');
            }

            //traitement des données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $poids = filter_var($poids, FILTER_SANITIZE_NUMBER_INT);
            $taille = filter_var($taille, FILTER_SANITIZE_NUMBER_INT);
            $pv = filter_var($pv, FILTER_SANITIZE_NUMBER_INT);
            $pdef = filter_var($pdef, FILTER_SANITIZE_NUMBER_INT);
            $pa = filter_var($pa, FILTER_SANITIZE_NUMBER_INT);
            $pAtt = filter_var($pAtt, FILTER_SANITIZE_NUMBER_INT);

            //enregistrement en bdd
            $perso = new Personnage();
            $perso->type = 0;
            $perso->nom = $nom;
            $perso->prenom = $prenom;
            $perso->poids = $poids;
            $perso->taille = $taille;
            $perso->pv = $pv;
            $perso->pAttaque = $pAtt;
            $perso->pAgilite = $pa;
            $perso->pDefense = $pdef;

            $persoExistant = $perso::where("nom", "=", $perso - nom)->first();

            if (!isset($persoExistant)) {
                //traitement des photos

                //enregistrement bdd
                $perso->save();
                if (!empty($_FILES["photo"]['name']) && isset($_FILES['photo']) && !empty($_FILES['photo'])) { //si oui
                    //permet de voir si un fichier a été chargé
                    if ($_FILES['photo']["error"] !== 4) {
                        //vérification pour voir s'il y a une erreur
                        if ($_FILES['photo']["error"] !== 0)
                            throw new \Exception("Erreur dans l'upload du fichier");

                        //vérification qu'il ait une extension parmi .jpg, .png, .gif ou .jpeg ou .Jpeg
                        $infosFichier = pathinfo($_FILES["photo"]["name"]);
                        $extension = $infosFichier["extension"];
                        $autorises = array("jpg", "jpeg", "Jpeg", "JPEG", "png", "Png", "PNG", "gif", "GIF");
                        if (!in_array($extension, $autorises))
                            throw new \Exception("Extension non autorisée");


                        unset($autorises);
                        unset($infosFichier);

                        //déplacement du fichier dans /assets/images/personnages/
                        $avatar = basename($perso->personnage_id . "." . $extension);
                        unset($extension);
                        move_uploaded_file($_FILES["photo"]["tmp_name"], $cheminAvatar . $avatar);
                        $perso->photo = $avatar;
                    }

                } else {
                    $perso->photo = "defaut.jpg";
                }
                $perso->save();

            }

            //libération mémoire
            if (isset($avatar)) unset($avatar);
            unset($perso);
            unset($pa);
            unset($pv);
            unset($pdef);
            unset($poids);
            unset($taille);
            unset($nom);
            unset($prenom);

            return $this->redirect($response, 'listLegumes');

        } catch (\Exception $e) {
            return $this->render($response,'Admin.creerPersonnage.hero.html.twig',[erreur => $e->getMessage(),
                type => 0,
                poids => $poids,
                pAgilite => $pa,
                taille => $taille,
                pv => $pv,
                pDefense => $pdef,
                prenom => $prenom,
                ]);
        }

    }

    /**
     * méthode qui permet d'afficher la page pour créer un monstre
     * @param $request
     * @param $response
     * @return mixed
     */
    public function creerMonstre($request, $response)
    {
        return $this->render($response, "Admin.creerPersonnage.monstre.html.twig");
    }

    /**
     * méthode qui permet d'ajouter un monstre en BDD
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function ajouterMonstre($request, $response)
    {
        //chemin où seront stockées les photos des monstres
        $cheminAvatar = $GLOBALS['PUBLIC'] . "images/monstres/";

        //récupération des données
        $nom = (isset($_POST['nom'])) ? $_POST["nom"] : null;
        $poids = (isset($_POST['poids'])) ? $_POST["poids"] : null;
        $taille = (isset($_POST['taille'])) ? $_POST["taille"] : null;
        $pv = (isset($_POST['pv'])) ? $_POST["pv"] : null;
        $pdef = (isset($_POST['pdef'])) ? $_POST["pdef"] : null;
        $pa = (isset($_POST['pa'])) ? $_POST["pa"] : null;
        $pAtt = (isset($_POST['pAtt'])) ? $_POST['pAtt'] : null;

        try {
            //verification des données
            if (!isset ($nom) || !isset ($poids) || !isset ($taille) || !isset ($pv) || !isset ($pdef) || !isset ($pa))
                throw new \Exception("Une donnée requise n'a pas été entrée !");

            //traitement des données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $poids = filter_var($poids, FILTER_SANITIZE_NUMBER_INT);
            $taille = filter_var($taille, FILTER_SANITIZE_NUMBER_INT);
            $pv = filter_var($pv, FILTER_SANITIZE_NUMBER_INT);
            $pdef = filter_var($pdef, FILTER_SANITIZE_NUMBER_INT);
            $pa = filter_var($pa, FILTER_SANITIZE_NUMBER_INT);
            $pAtt = filter_var($pAtt, FILTER_SANITIZE_NUMBER_INT);

            $persoDouble = Personnage::where("nom",$nom)->first();
            if (!empty($persoDouble)){
                throw new \Exception('Nom déjà utilisé !');
            }

            //enregistrement en bdd
            $perso = new Personnage();
            $perso->type = 1;
            $perso->nom = $nom;
            $perso->poids = $poids;
            $perso->taille = $taille;
            $perso->pv = $pv;
            $perso->pAttaque = $pAtt;
            $perso->pAgilite = $pa;
            $perso->pDefense = $pdef;

            $persoExistant = $perso::where("nom", "=", $perso->nom)->first();

            if (!isset($persoExistant)) {
                //traitement des photos

                //enregistrement bdd
                $perso->save();
                if (!empty($_FILES["photo"]['name']) && isset($_FILES['photo']) && !empty($_FILES['photo'])) { //si oui
                    //permet de voir si un fichier a été chargé
                    if ($_FILES['photo']["error"] !== 4) {
                        //vérification pour voir s'il y a une erreur
                        if ($_FILES['photo']["error"] !== 0)
                            throw new \Exception("Erreur dans l'upload du fichier");

                        //vérification qu'il ait une extension parmi .jpg, .png, .gif ou .jpeg ou .Jpeg
                        $infosFichier = pathinfo($_FILES["photo"]["name"]);
                        $extension = $infosFichier["extension"];
                        $autorises = array("jpg", "jpeg", "Jpeg", "JPEG", "png", "Png", "PNG", "gif", "GIF");
                        if (!in_array($extension, $autorises))
                            throw new \Exception("Extension non autorisée");


                        unset($autorises);
                        unset($infosFichier);

                        //déplacement du fichier dans /assets/images/personnages/
                        $avatar = basename($perso->personnage_id . ".". $extension);
                        unset($extension);
                        move_uploaded_file($_FILES["photo"]["tmp_name"], $cheminAvatar . $avatar);
                        $perso->photo = $avatar;
                    }

                } else {
                    $perso->photo = "defaut.jpg";
                }
                $perso->save();

            }

            //libération mémoire
            if (isset($avatar)) unset($avatar);
            unset($perso);
            unset($pa);
            unset($pv);
            unset($pdef);
            unset($poids);
            unset($taille);
            unset($nom);


        } catch (\Exception $e) {
            return $this->render($response,'Admin.creerPersonnage.monstre.html.twig',[erreur => $e->getMessage(),
                type => 1,
                poids => $poids,
                pAgilite => $pa,
                taille => $taille,
                pv => $pv,
                pDefense => $pdef,
            ]);
        }

        return $this->redirect($response, 'listFruits');

    }

    /**
     * méthode qui permet d'afficher la page pour modifier un utilisateur
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function modifAffichUtilisateur($request, $response, $args)
    {
        $user = null;
        //Récupération de l'utilisateur
        $id = isset($args['id']) ? $args['id'] : null;
        try {
            $user = Authentification::find($id);
            if (!isset($user)) {
                throw new \Exception("Utilisateur non trouvé.");
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        if (!$this->estCompteParDefaut($user)) {
            return $this->render($response, 'Admin.ModifUtilisateur.html.twig', ['id' => $user->authentification_id,
                "login" => $user->login,
                "niveau" => $user->niveau]);
        } else {
            return $this->redirect($response, "voirUtilisateurs");
        }
    }

    /**
     * méthode qui permet de modifier un utilisateur
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function modifUtilisateur($request, $response)
    {
        try {
            $login = (isset($_POST['login'])) ? $_POST["login"] : null;
            $password = (isset($_POST['password'])) ? $_POST["password"] : null;
            $niveau = (isset($_POST['niveau'])) ? $_POST["niveau"] : null;
            $id = (isset($_POST['id'])) ? $_POST["id"] : null;

            //traitement des données
            $niveau = filter_var($niveau, FILTER_SANITIZE_STRING);
            $login = filter_var($login, FILTER_SANITIZE_STRING);
            $password = filter_var($password, FILTER_SANITIZE_STRING);

            switch ($niveau) {
                case "Joueur":
                    $niveau = 1;
                    break;
                case "Administrateur":
                    $niveau = 0;
                    break;
                default:
                    $niveau = 1;
            }

            //Verification si utilisateur existe déjà
            $existUser = Authentification::where("login", $login)->first();
            if (!empty($existUser) && $existUser->authentification_id != $id) {
                throw new \Exception("Nom d'utilisateur déjà utilisé!");
            }

            $user = Authentification::find($id);
            $user->login = $login;
            $user->password = $this->saler($password);
            $user->niveau = $niveau;
            $user->save();

            unset($niveau);
            unset($password);
            unset($login);
            unset($user);

            return $this->redirect($response, "voirUtilisateurs");
        } catch (\Exception $e) {
           return $this->render($response, "Admin.ModifUtilisateur.html.twig", ['id' => $id, 'niveau' => $niveau, 'erreur' => $e->getMessage()]);
        }
    }

    /**
     * méthode qui permet de supprimer un utilisateur
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function supprUtilisateur($request, $response, $args)
    {
        $user = null;
        //Récupération de l'utilisateur
        $id = isset($args['id']) ? $args['id'] : null;
        try {
            $user = Authentification::find($id);
            if (!isset($user)) {
                throw new \Exception("Utilisateur non trouvé.");
            }
            if (!$this->estCompteParDefaut($user)) {
                $user->delete();
            }
            return $this->redirect($response, "voirUtilisateurs");
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * méthode privée qui sert à savoir si l'utilisateur connecté (en admin) n'est pas le compte admin fourni par défaut avec l'application
     * @param $compte
     * @return bool
     */
    private function estCompteParDefaut($compte)
    {
        $compteDefaut = Authentification::where("login", "admin")->first();
        if ($compte->login != $compteDefaut->login) {
            return false;
        }
        return true;
    }

}