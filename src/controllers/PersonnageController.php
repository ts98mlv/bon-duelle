<?php

namespace bonduelle\controllers;

use bonduelle\models\Personnage;
use Illuminate\Database\Capsule\Manager as DB;
use mysql_xdevapi\Exception;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class PersonnageController extends BaseController
{
    /**
     * méthode qui permet d'afficher la liste des personnages
     * @param $request
     * @param $response
     * @return mixed
     */
    public function index($request, $response) {
        $personnages = Personnage::all();

        return $this->views->render($response, 'personnage.list.html.twig', ['personnages' => $personnages]);
    }

    /**
     * méthode qui permet d'afficher la liste des personnages (Légumes)
     * @param $request
     * @param $response
     * @return mixed
     */
    public function indexLegume($request, $response){
        $personnages = Personnage::where('type','=','0')->get();

        return $this->render($response, 'personnage.list.html.twig', ['personnages' => $personnages]);
    }

    /**
     * méthode qui permet d'afficher la liste des monstres (fruits)
     * @param $request
     * @param $response
     * @return mixed
     */
    public function indexFruit($request, $response){
        $personnages = Personnage::where('type','=','1')->get();

        return $this->render($response, 'Monstre.list.html.twig', ['personnages' => $personnages]);
    }

    /**
     * méthode qui permet de supprimer un personnage
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function deletePerso($request, $response, $args) {
        $perso = Personnage::find($args["id"]);

        $type = $perso->type;

        if (isset($perso)) {
            if ($this->estPersonnageParDefaut($perso)) {
                switch ($type) {
                    case 0:
                        return $this->redirect($response, 'listLegumes');
                        break;
                    case 1:
                        return $this->redirect($response, 'listFruits');
                        break;
                    default:
                        break;
                }

            }
            //redirection de l'utilisateur en fonction du type de personnage supprimé
            switch ($type) {
                case 0:
                    if ($perso->photo != "defaut.jpg") {
                        $lienImage = $GLOBALS['PUBLIC'] . 'images/personnages/' . $perso->photo;
                        unlink($lienImage);
                    }
                    $perso->delete();
                    $route = $this->container->get('router')->pathFor('listLegumes');
                    return $response->withRedirect($route);
                    break;

                case 1:
                    //chemin où seront stockées les photos des monstres
                    if ($perso->photo != "defaut.jpg") {
                        $lienImage = $GLOBALS['PUBLIC'] . 'images/monstres/' . $perso->photo;
                        unlink($lienImage);
                    }
                    $perso->delete();
                    $route = $this->container->get('router')->pathFor('listFruits');
                    return $response->withRedirect($route);
                    break;

                default:
                    break;
            }
        }

        //redirection de l'utilisateur en fonction du type de personnage supprimé
        switch ($type) {
            case 0:
                $route = $this->container->get('router')->pathFor('listLegumes');
                return $response->withRedirect($route);
                break;

            case 1:
                $route = $this->container->get('router')->pathFor('listFruits');
                return $response->withRedirect($route);
                break;

            default:
                break;
        }
    }

    /**
     * méthode qui permet d'afficher la page de modification d'un personnage
     * @param $request
     * @param $response
     * @param $args
     * @return ResponseInterface
     */
    public function modifAffichPerso($request, $response, $args){

        $perso = null;
        //Récupération du perso
        $id = isset($args['id']) ? $args['id'] : null;

        try {
            $perso = Personnage::find($id);
            if (!isset($perso)) {
                throw new \Exception("Personnage non trouvé.");
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        if (!$this->estPersonnageParDefaut($perso)) {
            return $this->render($response, 'ModifierPerso.html.twig', ['identifiant' => $perso->personnage_id,
                'nom' => $perso->nom,
                'prenom' => $perso->prenom,
                'poids' => $perso->poids,
                'taille' => $perso->taille,
                'pv' => $perso->pv,
                'pDefense' => $perso->pDefense,
                'pAgilite' => $perso->pAgilite,
                'photo' => $perso->photo]);
        }
        return $this->redirect($response, 'listLegumes');
    }

    public function modifAffichMonstre($request, $response, $args)
    {
        $perso = null;
        //Récupération du perso
        $id = isset($args['id']) ? $args['id'] : null;
        try {
            $perso = Personnage::find($id);
            if (!isset($perso)) {
                throw new \Exception("Monstre non trouvé.");
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        if (!$this->estPersonnageParDefaut($perso)) {
            return $this->render($response, 'ModifierMonstre.html.twig', ['identifiant' => $perso->personnage_id,
                'nom' => $perso->nom,
                'poids' => $perso->poids,
                'taille' => $perso->taille,
                'pv' => $perso->pv,
                'pDefense' => $perso->pDefense,
                'pAgilite' => $perso->pAgilite,
                'photo' => $perso->photo]);
        }
        return $this->redirect($response, 'Monstre.list.html.twig');
    }

    /**
     * méthode qui permet de modifier un monstre
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function modifMonstre($request, $response){
        //chemin où seront stockées les photos des monstres
        $cheminAvatar = $GLOBALS['PUBLIC'] . 'images/monstres/';
        //récupération des données
        $nom = (isset($_POST['nom'])) ? $_POST["nom"] : null;
        $poids = (isset($_POST['poids'])) ? $_POST["poids"] : null;
        $taille = (isset($_POST['taille'])) ? $_POST["taille"] : null;
        $pv = (isset($_POST['pv'])) ? $_POST["pv"] : null;
        $pdef = (isset($_POST['pdef'])) ? $_POST["pdef"] : null;
        $pa = (isset($_POST['pa'])) ? $_POST["pa"] : null;
        $id = (isset($_POST['id'])) ? $_POST["id"] : null;

        try {
            //verification des données
            if (!isset ($nom) || !isset ($poids) || !isset ($taille) || !isset ($pv) || !isset ($pdef) || !isset ($pa))
                throw new \Exception("Une donnée requise n'a pas été entrée !");
            $perso = Personnage::find($id);
            if (!isset($perso)) {
                throw new \Exception("Monstre non trouvé.");
            }

            $persoDouble = Personnage::where("nom", $nom)->first();
            if (!empty($persoDouble) && $persoDouble->personnage_id != $perso->personnage_id) {
                throw new \Exception('Nom déjà utilisé !');
            }

            //traitement des données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $poids = filter_var($poids, FILTER_VALIDATE_FLOAT);
            $taille = filter_var($taille, FILTER_VALIDATE_INT);
            $pv = filter_var($pv, FILTER_VALIDATE_INT);
            $pdef = filter_var($pdef, FILTER_VALIDATE_INT);
            $pa = filter_var($pa, FILTER_VALIDATE_INT);

            //enregistrement en bdd
            $perso->nom = $nom;
            $perso->poids = $poids;
            $perso->taille = $taille;
            $perso->pv = $pv;
            $perso->pAgilite = $pa;
            $perso->pDefense = $pdef;

            //traitement des photos
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) { //si oui
                //permet de voir si un fichier a été chargé
                if ($_FILES['photo']["error"] !== 4) {
                    //vérification pour voir s'il y a une erreur
                    if ($_FILES['photo']["error"] !== 0)
                        throw new \Exception("Erreur dans l'upload du fichier");

                    //vérification qu'il ait une extension parmi .jpg, .png, .gif ou .jpeg ou .Jpeg
                    $infosFichier = pathinfo($_FILES["photo"]["name"]);
                    $extension = $infosFichier["extension"];
                    $autorises = array("jpg", "jpeg", "Jpeg", "JPEG", "png", "Png", "PNG", "gif", "GIF");
                    if (!in_array($extension, $autorises))
                        throw new \Exception("Extension non autorisée");


                    unset($autorises);
                    unset($infosFichier);

                    //déplacement du fichier dans /assets/images/monstres/
                    $avatar = basename($id . $extension);
                    unset($extension);
                    $boll = move_uploaded_file($_FILES["photo"]["tmp_name"], $cheminAvatar . $avatar);
                    $perso->photo = $avatar;
                }

            }

            //enregistrement bdd
            $perso->save();

            //libération mémoire
            if (isset($avatar)) unset($avatar);
            unset($perso);
            unset($pa);
            unset($pv);
            unset($pdef);
            unset($poids);
            unset($taille);
            unset($nom);

            return $this->redirect($response, 'listFruits');

        } catch (\Exception $e) {
            return $this->render($response, 'Admin.creerPersonnage.monstre.html.twig', [erreur => $e->getMessage(),
                type => 0,
                poids => $poids,
                pAgilite => $pa,
                taille => $taille,
                pv => $pv,
                pDefense => $pdef,
            ]);
        }


    }

    /**
     * méthode qui permet de modifier un personnage
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function modifPerso($request, $response){
        //chemin où seront stockées les photos des personnages
        $cheminAvatar = $GLOBALS['PUBLIC'] . 'images/personnages/';
        //récupération des données
        $nom = (isset($_POST['nom'])) ? $_POST["nom"] : null;
        $prenom = (isset($_POST['prenom'])) ? $_POST["prenom"] : null;
        $poids = (isset($_POST['poids'])) ? $_POST["poids"] : null;
        $taille = (isset($_POST['taille'])) ? $_POST["taille"] : null;
        $pv = (isset($_POST['pv'])) ? $_POST["pv"] : null;
        $pdef = (isset($_POST['pdef'])) ? $_POST["pdef"] : null;
        $pa = (isset($_POST['pa'])) ? $_POST["pa"] : null;
        $id = (isset($_POST['id'])) ? $_POST["id"] : null;

        try {
            //verification des données
            if (!isset ($nom) || !isset ($prenom) || !isset ($poids) || !isset ($taille) || !isset ($pv) || !isset ($pdef) || !isset ($pa))
                throw new \Exception("Une donnée requise n'a pas été entrée !");
            $perso = Personnage::find($id);
            if (!isset($perso)) {
                throw new \Exception("Personnage non trouvé.");
            }
            //traitement des données
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $poids = filter_var($poids, FILTER_VALIDATE_FLOAT);
            $taille = filter_var($taille, FILTER_VALIDATE_INT);
            $pv = filter_var($pv, FILTER_VALIDATE_INT);
            $pdef = filter_var($pdef, FILTER_VALIDATE_INT);
            $pa = filter_var($pa, FILTER_VALIDATE_INT);

            $persoDouble = Personnage::where("nom", $nom)->first();
            if (!empty($persoDouble) && $persoDouble->personnage_id != $perso->personnage_id) {
                throw new \Exception('Nom déjà utilisé !');
            }

            //enregistrement en bdd
            $perso->nom = $nom;
            $perso->prenom = $prenom;
            $perso->poids = $poids;
            $perso->taille = $taille;
            $perso->pv = $pv;
            $perso->pAgilite = $pa;
            $perso->pDefense = $pdef;

            //traitement des photos
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) { //si oui
                //permet de voir si un fichier a été chargé
                if ($_FILES['photo']["error"] !== 4) {
                    //vérification pour voir s'il y a une erreur
                    if ($_FILES['photo']["error"] !== 0)
                        throw new \Exception("Erreur dans l'upload du fichier");

                    //vérification qu'il ait une extension parmi .jpg, .png, .gif ou .jpeg ou .Jpeg
                    $infosFichier = pathinfo($_FILES["photo"]["name"]);
                    $extension = $infosFichier["extension"];
                    $autorises = array("jpg", "jpeg", "Jpeg", "JPEG", "png", "Png", "PNG", "gif", "GIF");
                    if (!in_array($extension, $autorises))
                        throw new \Exception("Extension non autorisée");


                    unset($autorises);
                    unset($infosFichier);

                    //déplacement du fichier dans /assets/images/personnages/
                    $avatar = basename($id . $extension);
                    unset($extension);
                    $boll = move_uploaded_file($_FILES["photo"]["tmp_name"], $cheminAvatar . $avatar);
                    $perso->photo = $avatar;
                }

            }

            //enregistrement bdd
            $perso->save();

            //libération mémoire
            if (isset($avatar)) unset($avatar);
            unset($perso);
            unset($pa);
            unset($pv);
            unset($pdef);
            unset($poids);
            unset($taille);
            unset($nom);
            unset($prenom);

            return $this->redirect($response, 'listLegumes');

        } catch (\Exception $e) {
            return $this->render($response, 'Admin.creerPersonnage.hero.html.twig', [erreur => $e->getMessage(),
                type => 1,
                poids => $poids,
                pAgilite => $pa,
                taille => $taille,
                pv => $pv,
                pDefense => $pdef,
                prenom => $prenom,
            ]);
        }


    }

    public function getHome(RequestInterface $request, ResponseInterface $response)
    {
        return $this->render($response, "Joueur.home.html.twig");
    }

/**
     * méthode privée qui permet de savoir si un personnage fait parti des personnages par défaut (fournis avec m'application)
     * @param $perso
     * @return bool
     */
    public function estPersonnageParDefaut($perso)
    {
        $tabPersosDefaut = array(
            "1" => Personnage::where("nom", "=", "Rapé")->where("prenom", "=", "Carotte")->first(),
            "2" => Personnage::where("nom", "=", "Frit")->where("prenom", "=", "Oignon")->first(),
            "3" => Personnage::where("nom", "=", "Ketchup")->where("prenom", "=", "Tomato")->first(),
            "4" => Personnage::where("nom", "=", "Rouge")->where("prenom", "=", "Poivron")->first(),
            "5" => Personnage::where("nom", "=", "Acerola")->first(),
            "6" => Personnage::where("nom", "=", "Banana")->first(),
            "7" => Personnage::where("nom", "=", "Dumber")->first(),
            "8" => Personnage::where("nom", "=", "LaRamenePas")->first(),
        );
        if (!in_array($perso, $tabPersosDefaut)) {
            return false;
        }
        return true;
    }

    public function getPersoWithID(RequestInterface $request, ResponseInterface $response, $args)
    {
        $perso = Personnage::find($args['id']);
        return json_encode($perso);
    }


    public function getPersonnages($twig, $response)
    {
        $persos = Personnage::where("type", 0)->get();
        $monstres = Personnage::where("type", 1)->get();
        $stats = array();

        foreach ($persos as $perso) {
            $stats[$perso->personnage_id] = StatsControlleur::getStats($perso);
        }

        foreach ($monstres as $monstre) {
            $stats[$monstre->personnage_id] = StatsControlleur::getStats($monstre);
        }

        return $this->render($response, $twig, ['persos' => $persos, 'monstres' => $monstres, "stats" => $stats]);
    }

    public function getChoix1vs1($request, $response)
    {
        $this->getPersonnages("Joueur.1vs1.html.twig", $response);
    }

    public function getChoix3vs3($request, $response)
    {
        $this->getPersonnages("Joueur.3vs3.html.twig", $response);
    }
}