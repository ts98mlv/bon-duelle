<?php


namespace bonduelle\controllers;

use bonduelle\models\Combat;
use bonduelle\models\Personnage;
use bonduelle\models\StatsCombatPerso;


class StatsControlleur extends AdminController
{

    /**
     * méthode qui permet d'afficher la liste des combats
     * @param $request
     * @param $response
     * @return mixed
     */
    public function showCombats($request, $response){
        $combats = Combat::all();
        $lesCombats = [];
        $perso = null;
        $monstre = null;

        foreach ($combats as $combat){
            $persos = Personnage::where("personnage_id", "=", $combat->protagoniste_id)
                ->orWhere("personnage_id", "=", $combat->protagoniste_id2)
                ->orWhere("personnage_id", "=", $combat->protagoniste_id3)->get();
            $monstres = Personnage::where("personnage_id", "=", $combat->monstre_id)
                ->orWhere("personnage_id", "=", $combat->monstre_id2)
                ->orWhere("personnage_id", "=", $combat->monstre_id3)->get();

            $lesCombats[$combat->combat_id] = [
              "id" => $combat->combat_id,
              "personnages" => $persos,
              "monstres" => $monstres,
              "finalite" => $combat->finalite
            ];
        }

        return $this->render($response, "Admin.voirCombats.html.twig", ["combats" => $lesCombats]);
    }

    /**
     * méthode qui permet d'avoir les statistiques d'un personnage (légume ou fruit)
     * @param $perso
     * @return array
     */
    public static function getStats($perso){
        $stats = [];
        try{
            //on vérifie que le paramètre est bien un personnage
            if(! $perso instanceof Personnage){
                throw new \Exception("Le paramètre doit être un personnage (monstre ou héro)");
            }


            if($perso->type == 0){
                //on recupere les combats gagnés en 3 fois car une seule requête apportait des bugs
                $nbVictoires1 = Combat::where("protagoniste_id", "=", $perso->personnage_id)->where("finalite", "=", 0)->get();
                $nbVictoires2 = Combat::where("protagoniste_id2", "=", $perso->personnage_id)->where("finalite", "=", 0)->get();
                $nbVictoires3 = Combat::where("protagoniste_id3", "=", $perso->personnage_id)->where("finalite", "=", 0)->get();

                $nbCombats = Combat::where("protagoniste_id", "=", $perso->personnage_id)
                    ->orWhere("protagoniste_id2", "=", $perso->personnage_id)
                    ->orWhere("protagoniste_id3", "=", $perso->personnage_id)->get();
            }
            else{
                $nbVictoires1 = Combat::where("monstre_id", "=", $perso->personnage_id)->where("finalite", "=", 1)->get();
                $nbVictoires2 = Combat::where("monstre_id2", "=", $perso->personnage_id)->where("finalite", "=", 1)->get();
                $nbVictoires3 = Combat::where("monstre_id3", "=", $perso->personnage_id)->where("finalite", "=", 1)->get();

                $nbCombats = Combat::where("monstre_id", "=", $perso->personnage_id)
                    ->orWhere("monstre_id2", "=", $perso->personnage_id)
                    ->orWhere("monstre_id3", "=", $perso->personnage_id)->get();
            }

            $nbCombats = round($nbCombats->count(), 2);
            $nbVictoires = round($nbVictoires1->count() + $nbVictoires2->count() + $nbVictoires3->count(), 2);
            $nbDefaites = round($nbCombats - $nbVictoires, 2);

            if($nbCombats > 0)
                $ratioVictoires = round($nbVictoires / $nbCombats, 2);
            else
                $ratioVictoires = 0;

            $pourcentageVictoires = $ratioVictoires * 100;

            $statsCombat = StatsCombatPerso::where("personnage_id", "=", $perso->personnage_id)->get();

            $nbAttEff = 0;
            $nbAttRecues = 0;
            $totalDegRecus = 0;
            $totalDegEff = 0;

            foreach ($statsCombat as $s){
                $nbAttEff += $s->nbAttEffectuees;
                $nbAttRecues += $s->nbAttRecues;
                $totalDegRecus += $s->totalDegInfliges;
                $totalDegEff += $s->totalDegRecu;
            }

            $stats = [
                "personnage_id" => $perso->personnage_id,
                "nbVictoires" => $nbVictoires,
                "nbCombats" => $nbCombats,
                "nbDefaites" => $nbDefaites,
                "ratioVictoires" => $ratioVictoires,
                "pourcentageVictoires" => $pourcentageVictoires,
                "nbAttEff" => $nbAttEff,
                "nbAttRecues" => $nbAttRecues,
                "totalDegRecus" => $totalDegRecus,
                "totalDegEff" => $totalDegEff
            ];

        }catch (\Exception $e){
            die($e->getMessage());
        }

        return $stats;
    }

    /**
     * méthode qui permet d'afficher les statistiques de tous les monstres (fruits)
     * @param $request
     * @param $response
     */
    public function voirStatsMonstres($request, $response){
        $monstres = Personnage::where("type", "=", 1)->get();
        $stats = [];

        foreach ($monstres as $monstre){
            $stats[$monstre->personnage_id] = self::getStats($monstre);
        }

        $this->render($response, "Monstre.stats.list.html.twig", ["stats" => $stats, "monstres" => $monstres]);
    }

    /**
     * méthode qui permet de voir les statistiques des personnages (légumes)
     * @param $request
     * @param $response
     */
    public function voirStatsPersos($request, $response){
        $persos = Personnage::where("type", "=", 0)->get();
        $stats = [];

        foreach ($persos as $perso){
            $stats[$perso->personnage_id] = self::getStats($perso);
        }

        $this->render($response, "personnage.stats.list.html.twig", ["stats" => $stats, "personnages" => $persos]);
    }

    /**
     * Insère en base de données les stats d'un perso pour un combat donné
     * @param $request
     * @param $response
     */
    public function creerStats($request, $response){
        //On récupère les données en JSON
        $json = $request->getBody();
        $data = json_decode($json);
        
        $idCombat = $data->combat_id;
        $idPerso = $data->personnage_id;
        $nbAttEffectuees = $data->nbAttEffectuees;
        $nbAttRecues = $data->nbAttRecues;
        $totalDegInfliges = $data->totalDegInfliges;
        $totalDegRecu = $data->totalDegRecu;

        //Insertion en BD
        $statPerso = StatsCombatPerso::create(['combat_id' => $idCombat,
                                                'personnage_id' => $idPerso,
                                                'nbAttEffectuees' => $nbAttEffectuees, 
                                                'nbAttRecues' => $nbAttRecues,
                                                'totalDegInfliges' => $totalDegInfliges,
                                                'totalDegRecu' => $totalDegRecu]);
               
    }

    /**
     * Fonction créannt deux tableaux, triés selon le ratio de victoire, l'un des personnages et monstres, et l'autre avec le ratio de victoire des dits personnages et/ou monstres.
     * @param $request
     * @param $response
     */
    public function creerClassement($request,$response){
        $persos = Personnage::all();
        $statsPersos = [];
        foreach ($persos as $perso){
            array_push($statsPersos,self::getStats($perso));
        }
        $tabClassement = [];
        foreach ($statsPersos as $stat){
            $tabClassement[$stat['personnage_id']] = $stat['ratioVictoires'];
        }
        unset($statsPersos);

        arsort($tabClassement);
        $tabClassementPerso = [];
        foreach ($tabClassement as $key => $value){
            foreach ($persos as $valPerso){
                if ($key == $valPerso->personnage_id){
                    $tabClassementPerso[$valPerso->personnage_id] = $valPerso;
                }
            }
        }

        $this->render($response, "Admin.classement.html.twig", ["stats" => $tabClassement, "personnages" => $tabClassementPerso]);


    }

}