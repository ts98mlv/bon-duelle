<?php


namespace bonduelle\controllers;

use bonduelle\models\Authentification;
use bonduelle\models\Combat;
use Illuminate\Database\Capsule\Manager as DB;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class AuthentificationController extends BaseController
{
    const ADMIN = 0;

    /**
     * méthode qui permet d'afficher la liste des comptes utilisateurs
     * @param $request
     * @param $response
     * @return mixed
     */
    public function Index($request, $response)
    {
        $users = Authentification::all();

        return $this->render($response, 'authentification.list.html.twig', ['users' => $users]);
    }

    /**
     * méthode qui permet d'afficher la page de connexion
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherConnexion($request, $response)
    {
        if (isset($_SESSION['user']))
            unset($_SESSION['user']);
        return $this->render($response, 'Login.html.twig', ["m2p" => password_hash("La nature, notre futur" . "admin", PASSWORD_DEFAULT) . "\n \n \n" . password_hash("La nature, notre futur" . "password", PASSWORD_DEFAULT)]);
    }


    /**
     * méthode qui permet de vérifier la conformité de deux mots de passe
     * @param $password_entry
     * @param $paswword_bdd
     * @return bool
     */
    private function verifyPassword($password_entry, $paswword_bdd)
    {
        return password_verify("La nature, notre futur" . $password_entry, $paswword_bdd);
    }

    /**
     * méthode qui permet de gérer l'authentification
     * @param $request
     * @param $response
     * @return ResponseInterface
     */
    public function authentifier($request, $response)
    {
        try {

            //récupération des données
            $login = isset($_POST["login"]) ? $_POST["login"] : null;
            $password = isset($_POST["password"]) ? $_POST["password"] : null;

            //envoie d'exception si au moins une des 2 donnees est manquante
            if (!isset($login) || !isset($password))
                throw new \Exception("Il faut remplir tous le champs!");

            //filtrage des données
            $login = filter_var($login, FILTER_SANITIZE_STRING);
            $password = filter_var($password, FILTER_SANITIZE_STRING);

            //vérification de l'existence de l'utilisateur
            $user = Authentification::where('login', '=', $login)->first();
            if (!isset($user))
                throw new \Exception("Le nom d'utilisateur n'existe pas!");

            // vérification du mot de passe
            if (!$this->verifyPassword($password, $user->password))
                throw new \Exception("Le mot de passe ne correspond pas!");

            //vérification des droits
            $isAdmin = false;
            if ($user->niveau == self::ADMIN)
                $isAdmin = true;

            //enregistrement en session
            if (isset($_SESSION['user']))
                unset($_SESSION['user']);
            $_SESSION['user'] = array(
                'id' => $user->authentification_id,
                'admin' => $isAdmin);

            //redirections
            if ($isAdmin)
                return $this->redirect($response, 'accueilAdmin');
            else
                return $this->redirect($response, 'accueilJoueur');

        } catch (\Exception $e) {
            $this->render($response, "Login.html.twig", [erreur => $e->getMessage()]);
        }
    }

    /**
     * fonction qui permet d'afficher la page d'accueil de l'administration
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    function voirAccueilAdmin(RequestInterface $request, ResponseInterface $response)
    {
        $nbCombats = Combat::all()->count();
        return $this->render($response, 'Admin.home.html.twig', ['nbCombats' => $nbCombats]);
    }


    /**
     * fonction qui permet d'afficher la page d'accueil du jeu
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    function voirAccueilJoueur(RequestInterface $request, ResponseInterface $response)
    {
        return $this->render($response, 'Joueur.home.html.twig');
    }

}