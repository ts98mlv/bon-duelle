<?php

namespace bonduelle\controllers;

use bonduelle\models\Combat;
use bonduelle\models\Personnage;
use Illuminate\Database\Capsule\Manager as DB;

class CombatController extends BaseController 
{
    /**
     * méthode qui permet de créer un combat
     * @param $request
     * @param $response
     * @return mixed
     */
    public function creerCombat($request, $response){
        //On récupère les ID des combattants
        $idLegume = isset($_POST["idLegume"]) ? $_POST["idLegume"] : null;
        $idFruit = isset($_POST["idFruit"]) ? $_POST["idFruit"] : null;

        $legume = Personnage::find($idLegume);
        $fruit = Personnage::find($idFruit);
        //On poursuit si les ID sont valides 
        if(isset($legume) && isset($fruit)){
            //TODO Creation du combat en BD
            $combat = Combat::create(['protagoniste_id' => $idLegume, 'monstre_id' => $idFruit]);
            return $this->render($response,'Joueur.combat.html.twig',['legume' => $legume, 'fruit' => $fruit,'idCombat' => $combat->combat_id]);
        }
    }
     
    /**
     * Création du combat 3v3 à partir de la page de sélection des persos
     */
    public function creerCombat3v3($request, $response){
        //On récupère les ID des combattants

        $idLegume1 = isset($_POST["idLegume1"]) ? $_POST["idLegume1"] : null;
        $idLegume2 = isset($_POST["idLegume2"]) ? $_POST["idLegume2"] : null;
        $idLegume3 = isset($_POST["idLegume3"]) ? $_POST["idLegume3"] : null;
        $idFruit1 = isset($_POST["idFruit1"]) ? $_POST["idFruit1"] : null;
        $idFruit2 = isset($_POST["idFruit2"]) ? $_POST["idFruit2"] : null;
        $idFruit3 = isset($_POST["idFruit3"]) ? $_POST["idFruit3"] : null;

        $legume = Personnage::find($idLegume1);
        $legume2 = Personnage::find($idLegume2);
        $legume3 = Personnage::find($idLegume3);
        $fruit = Personnage::find($idFruit1);
        $fruit2 = Personnage::find($idFruit2);
        $fruit3 = Personnage::find($idFruit3);
        //On poursuit si les ID sont valides 
        if(isset($legume) && isset($fruit) && isset($legume2) && isset($fruit2)&& isset($legume3) && isset($fruit3)){
            //TODO Creation du combat en BD
            $combat = Combat::create(['protagoniste_id' => $idLegume1,
                                        'protagoniste_id2' => $idLegume2,
                                        'protagoniste_id3' => $idLegume3,
                                        'monstre_id' => $idFruit1,
                                        'monstre_id2' => $idFruit2,
                                        'monstre_id3' => $idFruit3,]);
            return $this->render($response,'Joueur.combat3v3.html.twig',['legume' => $legume,
                                                                        'legume2' => $legume2,
                                                                        'legume3' => $legume3,    
                                                                        'fruit' => $fruit,
                                                                        'fruit2' => $fruit2,
                                                                        'fruit3' => $fruit3,
                                                                        'idCombat' => $combat->combat_id]);
        }
    }

    /**
     * Insère la team gagnante du combat en base de données.
     */
    public function addWinner($request, $response, $args) {
        $teamGagnante = $args["winnerType"];
        $idCombat = $args["idCombat"];
        $combat = Combat::find($idCombat);
        $finalite = null;
        if($teamGagnante ==="legume"){
            $finalite = 0;
        }
        else {
            $finalite = 1;
        }
        $combat->finalite = $finalite;
        $combat->save();
    }
}
