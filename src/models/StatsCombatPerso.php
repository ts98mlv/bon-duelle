<?php


namespace bonduelle\models;


use Illuminate\Database\Eloquent\SoftDeletes;

class StatsCombatPerso extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'STATSCOMBATPERSO';
    protected $primaryKey = 'statsCombatPerso_id';
    public $timestamps = false;
    protected $fillable = ['combat_id', 'personnage_id', 'nbAttEffectuees','nbAttRecues','totalDegInfliges','totalDegRecu'];
    use SoftDeletes;
    protected $dates = ['deleted_at'];


}