<?php


namespace bonduelle\models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Combat extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'COMBAT';
    protected $primaryKey = 'combat_id';
    public $timestamps = false;
    protected $fillable = ['protagoniste_id','monstre_id','protagoniste_id2','protagoniste_id3','monstre_id2','monstre_id3'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}