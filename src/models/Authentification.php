<?php


namespace bonduelle\models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Authentification extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'AUTHENTIFICATION';
    protected $primaryKey = 'authentification_id';
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}