<?php


namespace bonduelle\models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Personnage extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'PERSONNAGE';
    protected $primaryKey = 'personnage_id';
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}