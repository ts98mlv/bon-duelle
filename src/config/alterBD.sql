ALTER TABLE `COMBAT` ADD `protagoniste_id2` INT(50) NULL DEFAULT NULL AFTER `deleted_at`; 
ALTER TABLE `COMBAT` ADD `protagoniste_id3` INT(50) NULL DEFAULT NULL AFTER `deleted_at`; 
ALTER TABLE `COMBAT` ADD `monstre_id2` INT(50) NULL DEFAULT NULL AFTER `deleted_at`; 
ALTER TABLE `COMBAT` ADD `monstre_id3` INT(50) NULL DEFAULT NULL AFTER `deleted_at`; 

Alter table COMBAT ADD FOREIGN KEY (protagoniste_id2) references PERSONNAGE(personnage_id);
Alter table COMBAT ADD FOREIGN KEY (protagoniste_id3) references PERSONNAGE(personnage_id);
Alter table COMBAT ADD FOREIGN KEY (monstre_id2) references PERSONNAGE(personnage_id);
Alter table COMBAT ADD FOREIGN KEY (monstre_id3) references PERSONNAGE(personnage_id);