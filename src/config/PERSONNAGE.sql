SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO `PERSONNAGE` ( `nom`, `prenom`, `poids`, `taille`, `pv`, `pAttaque`, `pDefense`, `pAgilite`, `photo`, `type`) VALUES
( 'Rapé', 'Carotte', '1.00', 1, 500, 127, 10, 120, 'carotte.png', 0),
( 'Frit', 'Oignon', '21.00', 21, 600, 100, 5, 160, 'oignon.png', 0),
( 'Ketchup', 'Tomato', '10.00', 1, 450, 150, 20, 201, 'tomate.png', 0),
( 'Rouge', 'Poivron', '10.00', 1, 666, 120, 8, 60, 'poivron.png', 0),
( 'Acerola', NULL, '1.00', 1, 700, 93, 7, 80, 'cerise.png', 1),
( 'Banana', NULL, '1.00', 1, 500, 21, 12, 120, 'banane.png', 1),
( 'Dumber', NULL, '1.00', 1, 764, 21, 21, 160, 'poire.png', 1),
( 'LaRamenePas', NULL, '1.00', 1, 1200, 56, 3, 80, 'fraise.png', 1),
( 'Raisino', NULL, '16.00', 130, 1000, 25, 23, 187, 'raisin.png', 1),
( 'Froletto', NULL, '145.00', 20, 700, 3, 12, 250, 'fraise_violette.png', 1),
( 'Fraisoo', NULL, '250.00', 56, 880, 12, 40, 230, 'fraise_rose.png', 1),
( 'Schtroumphinette', NULL, '100.00', 26, 700, 21, 65, 110, 'fraise_bleue.png', 1),
( 'Plantinou', NULL, '69.00', 200, 650, 82, 29, 340, 'banane_verte.png', 1),
( 'OGMBanana', NULL, '10.00', 10, 800, 101, 10, 240, 'banane_bleue.png', 1),
( 'Sucrine', 'Tomato', '200.00', 50, 450, 37, 20, 130, 'tomate_jaune.png', 0),
( 'Poivron', 'Vert', '300.00', 80, 810, 47, 15, 60, 'poivron_vert.png', 0),
( 'Rond', 'John', '150.00', 76, 700, 72, 13, 110, 'poivron_jaune.png', 0),
( 'Vert', 'Poireau', '24.00', 250, 900, 125, 3, 160, 'poireau.png', 0),
( 'Carlito', 'Violetto', '99.00', 123, 610, 66, 11, 220, 'carotte_violette.png', 0),
('Albinos', 'Carlito', '69.00', 60, 512, 77, 9, 120, 'carotte_blanche.png', 0),
('Personnalisé', 'N1', '10.00', 10, 10, 10, 31, 80, 'defaut.jpg', 0),
('Personnalisé M1', NULL, '10.00', 10, 10, 10, 14, 51, 'defaut.jpg', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
