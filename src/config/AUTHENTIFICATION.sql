USE bonduelle;

INSERT INTO `AUTHENTIFICATION` (`login`, `password`, `niveau`) VALUES
('admin', '$2y$10$3a4bc07vTwnixNZ.9v4APOFbjwlQu7P9qcQHyPmk0eebAMsbhQcPi', 0),
('joueur', '$2y$10$POm2slORVM5PF919Uw2pc.xBjVZyPvkVBJ41TWUmxVdhgbpDUMsFK', 1);

