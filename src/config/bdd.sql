CREATE DATABASE IF NOT EXISTS `bonduelle`;
use `bonduelle`;

CREATE TABLE IF NOT EXISTS AUTHENTIFICATION(
  authentification_id int(50) AUTO_INCREMENT,
  login varchar(50),
  password varchar(500),
  niveau int(5),
  PRIMARY KEY (authentification_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS PERSONNAGE(
  personnage_id int(50) AUTO_INCREMENT,
  nom varchar(50),
  prenom varchar(50),
  poids decimal(10,2),
  taille int(5),
  pv int(5),
  pAttaque int(5),
  pDefense int(5),
  pAgilite int(5),
  photo varchar(100),
  type int(5),
  PRIMARY KEY (personnage_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS COMBAT(
  combat_id int(50) AUTO_INCREMENT,
  protagoniste_id int(50),
  monstre_id int(50),
  finalite int(5),
  FOREIGN KEY (protagoniste_id) references PERSONNAGE(personnage_id),
  FOREIGN KEY (protagoniste_id2) references PERSONNAGE(personnage_id),
  FOREIGN KEY (protagoniste_id3) references PERSONNAGE(personnage_id),
  FOREIGN KEY (monstre_id) references PERSONNAGE(personnage_id),
  FOREIGN KEY (monstre_id2) references PERSONNAGE(personnage_id),
  FOREIGN KEY (monstre_id3) references PERSONNAGE(personnage_id),
  PRIMARY KEY (combat_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS STATSCOMBATPERSO(
  statsCombatPerso_id int(50) AUTO_INCREMENT,
  combat_id int(50),
  personnage_id int(50),
  nbAttEffectuees int(50),
  nbAttRecues int(50),
  totalDegInfliges int(50),
  totalDegRecu int(50),
  foreign key (combat_id) references COMBAT(combat_id),
  foreign key (personnage_id) references PERSONNAGE(personnage_id),
  PRIMARY KEY (statsCombatPerso_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
