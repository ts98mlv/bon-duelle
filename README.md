# Bon-Duelle

Dans la table PERSONNAGE, les légumes (gentils) sont caractérisés par type = 0 et les fruits (méchants) sont caractérisés par type = 1.

Dans la table COMBAT, FINALITE vaut 0 ou 1 ; 0 si le légume (protagoniste) a gagné et 1 si le fruit a gagné.

En tant qu'admin, il n'est pas possible de supprimer le compte administrateur de base.

En tant qu'admin, il n'est pas possible de modifier et/ou supprimer les monstres par défaut ; afin de voir les possibilités de modifications et suppression, il est fourni deux personnages (un légume et un fruit) appelés "personnalisé" qui ne sont pas des personnages par défaut.

Les statistiques des personnages ne sont visibles que pour la sélection des personnages en 1v1. Cependant les commbats en 3v3 prennent bien en compte les statistiques des personnages et l'issue de ces combats influent sur ces statistiques (visibles aussi dans la partie admin).

##Login

### Admin

id : admin <br>
password : admin

### Joueur

id : joueur <br>
password : password


##Install

1 - Avoir un compte MySQL. <br>
2 - Executer <i>bonduelle.sql</i> <br>
3 - Dans <i>src/config</i>, renommer <i>exempleconfig.inc.php</i> en <i>config.inc.php</i> et y ajouter son identifiant et mot de passe pour MySQL dans les champs prédéfinis. <br>
