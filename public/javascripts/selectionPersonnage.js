/*
GESTION DE LA PAGE DE SELECTION DES UTILISATEURS
*/


/**
 * Selection aleatoire d'un personnage
 */
function randomPerso() {
    let nbPersos = $(".actif .carousel-indicators li").length
    let alea = Math.floor(Math.random() * Math.floor(nbPersos));
    while ($($(".actif .carousel-indicators li")[alea]).hasClass("active")) {
        alea = Math.floor(Math.random() * Math.floor(nbPersos));
    }
    $(".actif .carousel-indicators li")[alea].click()
}

function choisirPerso() {
    let suivant = $(".carousel.actif").next(".carousel");
    $(".carousel.actif").removeClass("actif");
    if (suivant.length != 0) {
        suivant.addClass("actif");
        $(".breadcrumb .active").next().addClass("active");
    } else {
        $(".breadcrumb .active").next().addClass("active");
        majPageValidation();
        afficherPageValidation();
    }
}

function pageIdPersonnageChoisi() {
    return $(".personnages .item.active").data("id");
}

function pageIdMonstreChoisi() {
    return $(".monstres .item.active").data("id");
}

function majPageValidation() {
    $(".validation_personnage").html($(".personnages .item.active .item_content_carte_gauche").clone());
    $(".validation_monstre").html($(".monstres .item.active .item_content_carte_gauche").clone());
}

function afficherPageValidation() {
    $(".contentButton.actif").removeClass("actif");
    $(".contentButton").last().addClass("actif");
    $(".validation").addClass("actif");
}


$(document).ready(function () {
    /**
     * Associer les ID des personnages choisis au champs cachés du form
     */
    document.getElementById("lancementCombat").addEventListener("click", (event)=> {
        document.getElementById("hiddenLegume").value = pageIdPersonnageChoisi();
        document.getElementById("hiddenFruit").value = pageIdMonstreChoisi();
    });

    $(".breadcrumb a").click(function () {
        $(".breadcrumb .active").removeClass("active");
        $(".actif").removeClass("actif");
        $(".contentButton.actif").removeClass("actif");
        switch ($(this).data("id")) {
            case 0 :
                $(".breadcrumb a").first().addClass("active");
                $(".contentButton").first().addClass("actif");
                $(".personnages").addClass("actif");
                break;
            case 1 :
                $(".breadcrumb a").first().addClass("active");
                $($(".breadcrumb a")[1]).addClass("active");
                $(".contentButton").first().addClass("actif");
                $(".monstres").addClass("actif");
                break;
            case 2 :
                $(".breadcrumb a").first().addClass("active");
                $($(".breadcrumb a")[1]).addClass("active");
                $(".breadcrumb a").last().addClass("active");
                $(".contentButton").last().addClass("actif");
                $(".carousel").removeClass("actif");
                $(".validation").addClass("actif");
                majPageValidation();
                afficherPageValidation();
                break;
        }
    });
});

/**
 * Associer les ID des personnages choisis au champs cachés du form
 
document.getElementById("lancementCombat").addEventListener("click", (event)=> {
    document.getElementById("hiddenLegume").value = pageIdPersonnageChoisi();
    document.getElementById("hiddenFruit").value = pageIdMonstreChoisi();
})

*/