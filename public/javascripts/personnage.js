export class Personnage {
    constructor(id,nom,prenom,poids,taille,pv,pAttaque,pDefense,pAgilite,type){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.poids = poids;
        this.taille = taille;
        this.pv = pv;
        this.pAttaque = pAttaque;
        this.pDefense = pDefense;
        this.pAgilite = pAgilite;
        this.type = type;
        this.isDefending = false;
        this.nbAttaquesInfligees = Number(0);
        this.nbAttaquesRecues = Number(0);
        this.degatsInfliges = Number(0);
        this.degatsRecus = Number(0);
    }

    isDead(){
        if(this.pv <= 0){ return true;}
        else { return false;}
    }

    /**
     * 
     * @param {*} cible 
     */
    attaquer(cible) {
        let reductionFromDefending = 0;
        if(cible.isDefending){
            reductionFromDefending = 0.25;
        }
        let degat = this.pAttaque * (1 - reductionFromDefending - (cible.pDefense/100));
        degat = degat.toFixed(0)
        cible.pv -= degat;

        //Ajout de l'attaque aux stats des persos concernés
        this.nbAttaquesInfligees++
        this.degatsInfliges += Number(degat)
        cible.nbAttaquesRecues++
        cible.degatsRecus += Number(degat)
        return degat;
    }

    /*
    *   Détermine qui le personnage va attaquer parmi tout les personnages.
    *   Sera utile pour les combats en équipe 
    */
    choisirCible(tableauPerso) {

        //On filtre les personnages de la même équipe que le personnage
        let tableauFiltre = tableauPerso.filter( perso => {
            return perso.type !== this.type;
        });

        //On filtre les personnages morts
        let tableauFinal = tableauFiltre.filter( perso => {
            return !perso.isDead()
        })

        //On choisit la cible au hasard
        let cible = tableauFinal[Math.floor(Math.random() * tableauFinal.length)];
        return cible;     
    }

    updateAffichage() {
        let divPerso = document.getElementById(this.id);
        let spanPV = divPerso.getElementsByClassName("pvactuel");
        spanPV[0].innerHTML = this.pv.toFixed(0);
    }

    modeDefense() {
        this.isDefending = true;
    }

    modeAttaque() {
        this.isDefending = false;
    }

    jouerTour(cible){
        if(!this.isDefending){
           return this.attaquer(cible);
        }
    }
}