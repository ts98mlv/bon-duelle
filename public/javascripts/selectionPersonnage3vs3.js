let tabMonstres = []; //tableau d'id monstres
let tabPersos = []; //tableau d'id persos

/*Attendre que document soit charge*/
$(document).ready(function () {
    $(".cbt").click(function () {
        gererAffichage(this);
    });

    /**
     * Associer les ID des personnages choisis au champs cachés du form pour le post
     */
    document.getElementById("lancementCombat").addEventListener("click", (event) => {
        // Gerer cas ou cbt non select
        if (tabMonstres.length !== 3 || tabPersos.length !== 3)
            randomPerso();
        document.getElementById("hiddenLegume1").value = tabPersos[0];
        document.getElementById("hiddenLegume2").value = tabPersos[1];
        document.getElementById("hiddenLegume3").value = tabPersos[2];
        document.getElementById("hiddenFruit1").value = tabMonstres[0];
        document.getElementById("hiddenFruit2").value = tabMonstres[1];
        document.getElementById("hiddenFruit3").value = tabMonstres[2];
    });
});

/**
 * Gestion de l'affichage des combattants choisi
 * @param cbt combattant a ajouter
 */
function gererAffichage(cbt) {
    let tabUse = tabPersos;
    if (!$(cbt).parent("div").hasClass("partieLegumes")) {
        tabUse = tabMonstres;
    }
    if (!tabUse.includes($(cbt).data("id"))) { //verifier si le click est sur un cbt deja selectionne
        if (tabUse.length < 3) { //gestion de l'affichage pour conserver que 3 persos max
            tabUse.push($(cbt).data("id"));
        } else {
            $(".cbt[data-id='" + tabUse[0] + "']").removeClass("actif");
            tabUse.shift();
            tabUse.push($(cbt).data("id"));
        }
        $(cbt).addClass("actif");
    } else {
        $(cbt).removeClass("actif");
        tabUse.splice(tabUse.indexOf($(cbt).data("id")), 1);
    }
}

/**
 * Generation aleatoire de choix de combattants
 */
function randomPerso() {
    tabMonstres = [];
    tabPersos = [];
    $(".actif").removeClass("actif");
    let nbPersos = $(".partieLegumes .cbt").length;
    let nbMonstres = $(".partieFruits .cbt").length;

    let numAddCbt = [];
    let num;
    while (numAddCbt.length !== 3) {
        num = getRandomInt(nbPersos);
        if (!numAddCbt.includes(num)) {
            numAddCbt.push(num)
            $($(".partieLegumes .cbt")[num]).click();
        }
    }
    numAddCbt = [];
    while (numAddCbt.length !== 3) {
        num = getRandomInt(nbMonstres);
        if (!numAddCbt.includes(num)) {
            numAddCbt.push(num)
            $($(".partieFruits .cbt")[num]).click();
        }
    }
}

/**
 * Methode qui genere un random
 * @param max valeur max du random
 * @returns {number} val entre 0 et max exclu
 */
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}