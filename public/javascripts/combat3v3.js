import {Personnage} from "./personnage.js";

const persosID = []
// On récupère les ID des légumes
let legumesContainers = document.getElementsByClassName("legumeContainer");
let legumesID = [];
for (let leg of legumesContainers) {
    legumesID.push(leg.id)
    persosID.push(leg.id)
}
// On récupère les ID des légumes
let fruitContainers = document.getElementsByClassName("fruitContainer");
let fruitsID = [];
for (let fruit of fruitContainers) {
    fruitsID.push(fruit.id)
    persosID.push(fruit.id)
}


const personnages = [];
let persoActif;
let ordrePersonnages;
const teamLegume = [];
const teamFruit = [];
const idCombat = document.getElementById("idCombat").textContent;
setUpGame()

/**
 * On associe les actions à chaque bouton
 */

document.getElementsByClassName("attaquer")[0].addEventListener("click", (event) => {
    persoActif.modeAttaque();
    $(".active").removeClass("active");
    $(".attaquer").addClass("active");
})

document.getElementsByClassName("defendre")[0].addEventListener("click", (event) => {
    persoActif.modeDefense();
    $(".active").removeClass("active");
    $(".defendre").addClass("active");
})


document.getElementsByClassName("jouer")[0].addEventListener("click", eventListenerJouer)

function eventListenerJouer(event) {
    addToHistory()
    deleteHighlight();
    ordrePersonnages.shift();
    updateAffichage();

    //On vérifie que le prochain personnage soit en vie avant de jouer
    persoActif = ordrePersonnages[0];
    while (persoActif.isDead()) {
        ordrePersonnages.shift();
        persoActif = ordrePersonnages[0];
    }
    // On détermine de facon aléatoire si le nouveau personnage est en attaque ou défense au départ pour faire en sorte que les monstres n'attaquent pas 100% du temps.
    let rand = Math.random();
    if (rand < 0.7) {
        persoActif.modeAttaque();
    } else {
        persoActif.modeDefense();
    }
    highlightPersoActif();
    if (isCombatOver()) {
        finDePartie()
    }

    $(".active").removeClass("active");
}


/**
 * Vérifie que l'équipe en paramètre a toujours au moins un membre en vie
 */
function isTeamDead(team) {
    let isDead = true;
    team.forEach(element => {
        if (!element.isDead()) {
            isDead = false;
        }
    });
    return isDead;
}


function isCombatOver() {
    return isTeamDead(teamLegume) || isTeamDead(teamFruit)
}

/*
*Détermine l'ordre dans lequel les personnages vont jouer pour un tour de jeu.
*Dépend des points d'agilité des personnages.
*Pour chaque 50 points d'agilité d'un perso, il aura une action dans le tour.
*Ensuite on randomise l'ordre dans lequel les actions se passent pour tout le monde.
*/
function genererOrdre(tableau) {
    //TODO
    //sequence contiendra les personnages ordonnés
    let sequence = [];
    for (const char of tableau) {
        for (let index = 0; index < (char.pAgilite) / 50; index++) {
            sequence.push(char)
        }
    }

    return shuffle(sequence);
}

/**
 * Randomise l'ordre des éléments dans un tableau
 */
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // Tant que des éléments n'ont pas été shuffle
    while (0 !== currentIndex) {

        // On prend un élément que l'on doit shuffle
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // On swap
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

/**
 * Génère tout les tours de combats qui seront nécessaires à la partie
 */
function genererTours(tableau) {
    const sequenceTour = genererOrdre(personnages);
    ordrePersonnages = []

    for (let i = 0; i < 300; i++) {
        ordrePersonnages.push(sequenceTour[i % sequenceTour.length]);
    }
    return ordrePersonnages;
}

/**
 * Met à jour l'affichage des PV des personnages
 */
function updateAffichage() {
    personnages.forEach(element => {
        element.updateAffichage();
        if (element.pv <= 0) {
            let divDuPerso = document.getElementById(element.id)
            divDuPerso.getElementsByTagName("img")[0].src = "../public/images/rip.png"
        }
    });
}

/**
 * On active un affichage particulier pour le joueur actif
 */
function highlightPersoActif() {

    // On prend le container du personnage actif
    let divPerso = document.getElementById(persoActif.id)
    divPerso.classList.add("breathing")
    let boutons = document.getElementsByClassName("button")
    for (let item of boutons) {
        if (persoActif.type === 0) {
            item.disabled = false;
        } else {
            item.disabled = true;
        }
    }
}

/**
 * Enlève l'affichage du joueur actif
 */
function deleteHighlight() {

    let divPerso = document.getElementById(persoActif.id)
    divPerso.classList.remove("breathing")
}

/**
 * On récupère les données d'un personnage sur le serveur
 * @param {*} id
 */
async function fetchPerso(id) {
    let response = await fetch('getPerso/' + id, {
        method: "GET",
        headers: {"Content-Type": "application/json"}
    });
    let data = response.json();
    return data;
}

/**
 * On fetch tout les persos puis crée l'ordre dans lequel les personnages vont jouer avant de commencer la partie
 */
async function setUpGame() {
    for (let id of persosID) {
        await fetchPerso(id).then((data) => {
            let nouveauPerso = new Personnage(id, data.nom, data.prenom, data.poids, data.taille, data.pv, data.pAttaque, data.pDefense, data.pAgilite, data.type)
            personnages.push(nouveauPerso)
            if (data.type === 0) {
                teamLegume.push(nouveauPerso)
            }
            if (data.type === 1) {
                teamFruit.push(nouveauPerso)
            }
        });
    }
    genererTours(personnages)
    persoActif = ordrePersonnages[0];
    highlightPersoActif();
}

/**
 * On envoie les stats dans la BD et passe à l'affichage de fin de partie
 */
function finDePartie() {
    const boutonJouer = document.getElementsByClassName("jouer")[0]

    // Desactive le bouton pendant un certain temps a la fin de la partie pour
    // eviter de revenir sur la page d'accueil sans le vouloir après avoir spam combat
    boutonJouer.classList.add("disabled");
    setTimeout(function () {
        boutonJouer.classList.remove("disabled");
        boutonJouer.addEventListener("click", () => {
            window.location.href = "./home";
        })
    }, 2000);

    for (const perso of personnages) {
        const divDuPerso = document.getElementById(perso.id)
        let divUpdate = divDuPerso.getElementsByClassName("actionsPerso")[0]
        divUpdate.innerHTML = "";
        let pAttaqueInf = document.createElement("p")
        pAttaqueInf.appendChild(document.createTextNode("Attaques infligées : " + perso.nbAttaquesInfligees))
        divUpdate.appendChild(pAttaqueInf)
        let pDegatInf = document.createElement("p")
        pDegatInf.appendChild(document.createTextNode("Dégats infligés : " + perso.degatsInfliges))
        divUpdate.appendChild(pDegatInf)
        let pAttaqueRecues = document.createElement("p")
        pAttaqueRecues.appendChild(document.createTextNode("Attaques reçues : " + perso.nbAttaquesRecues))
        divUpdate.appendChild(pAttaqueRecues)
        let pDegatRecus = document.createElement("p")
        pDegatRecus.appendChild(document.createTextNode("Dégats reçus : " + perso.degatsRecus))
        divUpdate.appendChild(pDegatRecus)
        insertStatPerso(perso)

    }
    addWinnerToDB()
    boutonJouer.removeEventListener("click", eventListenerJouer)
    deleteHighlight()
    boutonJouer.textContent = "RETOUR A L'ACCUEIL";
}

/**
 * Joue le tour et l'ajoute à l'historique
 *
 */
function addToHistory() {
    let affichagePerso = persoActif.nom + " " + (persoActif.prenom ? persoActif.prenom : "");
    let actionTour = "";
    let cible = persoActif.choisirCible(personnages)
    let degat = persoActif.jouerTour(cible);
    if (persoActif.isDefending) {
        actionTour += " Se défend"
    } else {
        actionTour += " Attaque " + cible.nom + " " + (cible.prenom ? cible.prenom : "") + " : " + degat + " dégats"
    }
    let divHistory = document.getElementsByClassName("history")[0]
    let p = document.createElement("p")
    let span = document.createElement("span")
    span.classList.add("nomDuPerso")
    span.appendChild(document.createTextNode(affichagePerso))
    p.appendChild(span)
    p.appendChild(document.createTextNode(actionTour))
    divHistory.appendChild(p)
    updateScroll()
}

/**
 * Permet à la console de log de rester ancrée en bas
 */
function updateScroll() {
    const element = document.getElementsByClassName("history")[0];
    element.scrollTop = element.scrollHeight;
}

/**
 * Send the GET request to insert the winner into the DB
 */
function addWinnerToDB() {
    let winnerType;
    let teamWinner;
    for (const perso of personnages) {
        if (perso.pv > 0) {
            winnerType = perso.type;
        }
    }
    if (winnerType === 0) {
        teamWinner = "legume"
    } else {
        teamWinner = "fruit"
    }
    fetch('addWinner/' + idCombat + '/' + teamWinner, {
        method: "GET"
    });
}

/**
 * Envoie une requête sur la route d'insertion des stats de perso
 */
function insertStatPerso(perso) {
    fetch('creerStatPerso', {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            combat_id: idCombat,
            personnage_id: perso.id,
            nbAttEffectuees: perso.nbAttaquesInfligees,
            nbAttRecues: perso.nbAttaquesRecues,
            totalDegInfliges: perso.degatsInfliges,
            totalDegRecu: perso.degatsRecus
        })
    })
}