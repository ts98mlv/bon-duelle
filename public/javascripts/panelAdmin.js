$(document).ready(function () {

    //Gestion click navbar
    $(".navbar-primary-menu li a").click(function () {
        switch ($(this).data("btn")) {
            case "addAccount":
                $("iframe").attr("src", "creerCompte");
                break;
            case "modifyAccount":
                $("iframe").attr("src", "voirUtilisateurs");
                break;
            case "addPerso":
                $("iframe").attr("src", "creerPersonnage");
                break;
            case "modifyPerso":
                $("iframe").attr("src", "listLegumes");
                break;
            case "addMonstre":
                $("iframe").attr("src", "creerMonstre");
                break;
            case "modifyMonstre":
                $("iframe").attr("src", "listFruits");
                break;
            case "showCombats":
                $("iframe").attr("src", "listCombats");
                break;
            case "statsPersos":
                $("iframe").attr("src", "statsPersos");
                break;
            case "statsMonstres":
                $("iframe").attr("src", "statsMonstres");
                break;
            case "classement":
                $("iframe").attr("src", "classement");
                break;
        }
    });
});